add_library(pico_sj_libs_included INTERFACE)
target_compile_definitions(pico_sj_libs_included INTERFACE
        -DPICO_SJ_LIBS=1
        )

pico_add_platform_library(pico_sj_libs_included)

add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/src ${CMAKE_BINARY_DIR}/sj-libs/src)

if (SJ_LIBS_TESTS_ENABLED OR SJ_LIBS_TOP_LEVEL_PROJECT)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/test ${CMAKE_BINARY_DIR}/sj-libs/test)
endif ()

if (SJ_LIBS_EXAMPLES_ENABLED OR SJ_LIBS_TOP_LEVEL_PROJECT)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/examples ${CMAKE_BINARY_DIR}/examples)
endif ()