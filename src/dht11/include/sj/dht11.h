#ifndef _SJ_LIBS_DHT11_H
#define _SJ_LIBS_DHT11_H

#define DHT_PIN 15
#define DHT_MAX_TIMINGS 85

typedef struct {
    int humidity;
    int temp_celsius;
    int temp_celsius_decimal;
} dht_reading;

void dht_init();
void dht_read(dht_reading *result);

#endif