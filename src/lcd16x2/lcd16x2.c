#include "sj/lcd16x2.h"
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/i2c.h"


// commands
static const int LCD_CLEARDISPLAY = 0x01;
static const int LCD_RETURNHOME = 0x02;
static const int LCD_ENTRYMODESET = 0x04;
static const int LCD_DISPLAYCONTROL = 0x08;
static const int LCD_CURSORSHIFT = 0x10;
static const int LCD_FUNCTIONSET = 0x20;
static const int LCD_SETCGRAMADDR = 0x40;
static const int LCD_SETDDRAMADDR = 0x80;

// flags for display entry mode
static const int LCD_ENTRYSHIFTINCREMENT = 0x01;
static const int LCD_ENTRYLEFT = 0x02;

// flags for display and cursor control
static const int LCD_BLINKON = 0x01;
static const int LCD_CURSORON = 0x02;
static const int LCD_DISPLAYON = 0x04;

// flags for display and cursor shift
static const int LCD_MOVERIGHT = 0x04;
static const int LCD_DISPLAYMOVE = 0x08;

// flags for function set
static const int LCD_5x10DOTS = 0x04;
static const int LCD_2LINE = 0x08;
static const int LCD_8BITMODE = 0x10;

// flag for backlight control
static const int LCD_BACKLIGHT = 0x08;

static const int LCD_ENABLE_BIT = 0x04;

// By default these LCD display drivers are on bus address 0x27
static const int LCD_ADDRESS = 0x27;

// Modes for lcd_send_byte
static const int LCD_CHARACTER = 1;
static const int LCD_COMMAND = 0;
#define DELAY_US 600

/* Quick helper function for single byte transfers */
static void inline i2c_write_byte(uint8_t val) {
    i2c_write_blocking(i2c_default, LCD_ADDRESS, &val, 1, false);
}

static void lcd_toggle_enable(uint8_t val) {
    // Toggle enable pin on LCD display
    // We cannot do this too quickly or things don't work
    sleep_us(DELAY_US);
    i2c_write_byte(val | LCD_ENABLE_BIT);
    sleep_us(DELAY_US);
    i2c_write_byte(val & ~LCD_ENABLE_BIT);
    sleep_us(DELAY_US);
}

// The display is sent a byte as two separate nibble transfers
static void lcd_send_byte(uint8_t val, int mode) {
    uint8_t high = mode | (val & 0xF0) | LCD_BACKLIGHT;
    uint8_t low = mode | ((val << 4) & 0xF0) | LCD_BACKLIGHT;

    i2c_write_byte(high);
    lcd_toggle_enable(high);
    i2c_write_byte(low);
    lcd_toggle_enable(low);
}

void lcd_init() {
#if !defined(i2c_default) || !defined(PICO_DEFAULT_I2C_SDA_PIN) || !defined(PICO_DEFAULT_I2C_SCL_PIN)
    #error lcd16x2 lib requires a board with I2C pins
#endif
    i2c_init(i2c_default, 100 * 1000);
    gpio_set_function(PICO_DEFAULT_I2C_SDA_PIN, GPIO_FUNC_I2C);
    gpio_set_function(PICO_DEFAULT_I2C_SCL_PIN, GPIO_FUNC_I2C);
    gpio_pull_up(PICO_DEFAULT_I2C_SDA_PIN);
    gpio_pull_up(PICO_DEFAULT_I2C_SCL_PIN);
    lcd_send_byte(0x03, LCD_COMMAND);
    lcd_send_byte(0x03, LCD_COMMAND);
    lcd_send_byte(0x03, LCD_COMMAND);
    lcd_send_byte(0x02, LCD_COMMAND);

    lcd_send_byte(LCD_ENTRYMODESET | LCD_ENTRYLEFT, LCD_COMMAND);
    lcd_send_byte(LCD_FUNCTIONSET | LCD_2LINE, LCD_COMMAND);
    lcd_send_byte(LCD_DISPLAYCONTROL | LCD_DISPLAYON, LCD_COMMAND);
    lcd_clear();
}

void lcd_clear(void) {
    lcd_send_byte(LCD_CLEARDISPLAY, LCD_COMMAND);
}

// go to location on LCD
void lcd_set_cursor(int line, int position) {
    int val = (line == 0) ? 0x80 + position : 0xC0 + position;
    lcd_send_byte(val, LCD_COMMAND);
}

inline void lcd_char(char val) {
    lcd_send_byte(val, LCD_CHARACTER);
}

void lcd_string(const char *s) {
    while (*s) {
        lcd_char(*s++);
    }
}

void lcd_number_int(int i) {
    // if(reading.temp_celsius >= 10) {
    //     lcd_char(reading.temp_celsius / 10 + '0');
    // }
    // lcd_char(reading.temp_celsius % 10 + '0');
}
