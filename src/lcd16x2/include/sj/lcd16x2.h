#ifndef _SJ_LIBS_LCD16X2_H
#define _SJ_LIBS_LCD16X2_H


#define SJ_LCD_MAX_LINES = 2;
#define SJ_LCD_MAX_CHARS = 16;


void lcd_init();
void lcd_clear();
void lcd_set_cursor(int line, int position);
void lcd_char(char val);
void lcd_string(const char *s);


#endif