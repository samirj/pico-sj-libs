#ifndef _SJ_LIBS_DHT11_PIO_H
#define _SJ_LIBS_DHT11_PIO_H

#include "hardware/pio.h"

void dht11_program_init(PIO pio, uint sm, uint pin, void (*callback)(int, int, int));
void dht11_start_reading();

#endif