#include <stdio.h>
#include "sj/dht11pio.h"
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "dht11.pio.h"

static void (*_callback_ptr)(int, int, int) = 0;
static PIO _pio;
static uint _sm;
static uint _offset;
static pio_sm_config _config;

void dht11_irq() {
    pio_interrupt_clear(_pio, 0);
    irq_clear(PIO0_IRQ_0);
    pio_sm_set_enabled(_pio, _sm, false);
    
    int fifo_size = pio_sm_get_rx_fifo_level(_pio, _sm);
    printf("Fifo size: %d\n", fifo_size);

    if(fifo_size != 5) {
        printf("Fifo size invalid: %d\n", fifo_size);
        return;
    }

    int data[5] = {0, 0, 0, 0, 0};
    for(int i = 0; i < fifo_size; i++) {
        uint32_t d = pio_sm_get(_pio, _sm);
        printf("data[%d]=%d\n", i, d);
        data[i] = d;
    }

    //checksum
    if (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) {
        (*_callback_ptr)(data[0], data[2], data[3]);
    } else {
        printf("invalid checksum\n");
        (*_callback_ptr)(data[0], data[2], data[3]);
    }
}

void dht11_program_init(PIO pio, uint sm, uint pin, void (*callback)(int, int, int)) {

    irq_set_exclusive_handler(PIO0_IRQ_0, dht11_irq);
    irq_set_enabled(PIO0_IRQ_0, true);
    pio_set_irq0_source_enabled(pio, pis_interrupt0, true);

    _callback_ptr = callback;
    _pio = pio;
    _sm = sm;

    _offset = pio_add_program(pio, &dht11_program);
    _config = dht11_program_get_default_config(_offset);
    sm_config_set_in_pins(&_config, pin);
    sm_config_set_set_pins(&_config, pin, 1);
    sm_config_set_fifo_join(&_config, PIO_FIFO_JOIN_RX);
    sm_config_set_in_shift(&_config, false, true, 8);

    float d = clock_get_hz(clk_sys) / 1000000.0; // want to have 1us cycles
    sm_config_set_clkdiv(&_config, d);
    pio_gpio_init(pio, pin);
}

void dht11_start_reading() {
    pio_sm_init(_pio, _sm, _offset, &_config);
    pio_sm_set_enabled(_pio, _sm, true);
}
