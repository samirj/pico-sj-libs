#include "sj/utils.h"
#include "pico/stdlib.h"

void onboard_led_init() {
    gpio_init(PICO_DEFAULT_LED_PIN);
    gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
}

void onboard_led_on() {
    gpio_put(PICO_DEFAULT_LED_PIN, 1);
}

void onboard_led_off() {
    gpio_put(PICO_DEFAULT_LED_PIN, 0);
}

void onboard_led_toggle() {
    gpio_put(PICO_DEFAULT_LED_PIN, !gpio_get(PICO_DEFAULT_LED_PIN));
}
