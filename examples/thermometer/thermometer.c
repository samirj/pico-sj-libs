#include <stdio.h>
#include "pico/stdlib.h"
#include "sj/utils.h"
#include "sj/lcd16x2.h"
#include "sj/dht11.h"

int main()
{
    stdio_init_all();
    onboard_led_init();
    lcd_init();
    dht_init();

    while (true) {
        onboard_led_on();
        sleep_ms(100);
        onboard_led_off();

        dht_reading reading;
        dht_read(&reading);

        lcd_set_cursor(0, 0);
        lcd_string("Temp: ");
        if(reading.temp_celsius < 0) {
            lcd_char('-');
        }
        
        if(reading.temp_celsius >= 10) {
            lcd_char(reading.temp_celsius / 10 + '0');
        }
        lcd_char(reading.temp_celsius % 10 + '0');
        lcd_char('.');
        lcd_char(reading.temp_celsius_decimal % 10 + '0');
        lcd_char(0xDF);
        lcd_string("C  ");

        lcd_set_cursor(1, 0);
        lcd_string("Humidity: ");
        if(reading.humidity >= 10) {
            lcd_char(reading.humidity / 10 + '0');
        }
        lcd_char(reading.humidity % 10 + '0');
        lcd_char('%');

        printf("%d.%d *C, %d%%\n", reading.temp_celsius, reading.temp_celsius_decimal, reading.humidity);
        sleep_ms(5000);
    }
}
