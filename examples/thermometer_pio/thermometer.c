#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "hardware/timer.h"
#include "sj/utils.h"
#include "sj/lcd16x2.h"
#include "sj/dht11pio.h"


int humidity = 0;
int temp = 0;
int temp_dec = 0;


void callback(int h, int t, int t_dec) {
    humidity = h;
    temp = t;
    temp_dec = t_dec;
}

int main() {
    stdio_init_all();
    lcd_init();
    onboard_led_init();
    onboard_led_on();

    PIO pio = pio0;
    uint sm = pio_claim_unused_sm(pio, true);

    dht11_program_init(pio, sm, 15, &callback);
    
        printf("Starting the main loop\n");
    while (true) {
        printf("Starting the reading %d\n", sm);
        onboard_led_on();
        dht11_start_reading();
        sleep_ms(100);
        onboard_led_off();


        


        lcd_set_cursor(0, 0);
        lcd_string("Temp: ");
        if(temp < 0) {
            lcd_char('-');
        }
        
        if(temp >= 10) {
            lcd_char(temp / 10 + '0');
        }
        lcd_char(temp % 10 + '0');
        lcd_char('.');
        lcd_char(temp_dec % 10 + '0');
        lcd_char(0xDF); // *
        lcd_string("C  ");

        lcd_set_cursor(1, 0);
        lcd_string("Humidity: ");
        if(humidity >= 10) {
            lcd_char(humidity / 10 + '0');
        }
        lcd_char(humidity % 10 + '0');
        lcd_char('%');

        printf("%d.%d *C, %d%%\n", temp, temp_dec, humidity);
        
                sleep_ms(5000);
        
    }
    
}
