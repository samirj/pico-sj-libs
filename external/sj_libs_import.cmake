# This is a copy of <SJ_LIBS_PATH>/external/sj_libs_import.cmake

if (DEFINED ENV{SJ_LIBS_PATH} AND (NOT SJ_LIBS_PATH))
    set(SJ_LIBS_PATH $ENV{SJ_LIBS_PATH})
    message("Using SJ_LIBS_PATH from environment ('${SJ_LIBS_PATH}')")
endif ()

if (NOT SJ_LIBS_PATH)
    if (PICO_SDK_PATH AND EXISTS "${PICO_SDK_PATH}/../pico-sj-libs")
        set(SJ_LIBS_PATH ${PICO_SDK_PATH}/../pico-sj-libs)
        message("Defaulting SJ_LIBS_PATH as sibling of PICO_SDK_PATH: ${SJ_LIBS_PATH}")
    else()
        message(FATAL_ERROR
         "SJ LIBS location was not specified. Please set SJ_LIBS_PATH.")
    endif()
endif ()

set(SJ_LIBS_PATH "${SJ_LIBS_PATH}" CACHE PATH "Path to the PICO SJ LIBS")


get_filename_component(SJ_LIBS_PATH "${SJ_LIBS_PATH}" REALPATH BASE_DIR "${CMAKE_BINARY_DIR}")
if (NOT EXISTS ${SJ_LIBS_PATH})
    message(FATAL_ERROR "Directory '${SJ_LIBS_PATH}' not found")
endif ()

set(SJ_LIBS_PATH ${SJ_LIBS_PATH} CACHE PATH "Path to the PICO SJ LIBS" FORCE)

add_subdirectory(${SJ_LIBS_PATH} sj-libs)